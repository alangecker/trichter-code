import * as fs from 'fs'
import * as path from 'path'
import * as Twig from 'twig';
import * as moment from 'moment'

import Group from './lib/Group'
import Event from './lib/Event'
import {groupEventsByDays} from './lib/utils'

moment.locale('de')

// custom filters
Twig.extendFilter('isToday', (value) => moment(value).isSame(moment(), 'day'))
Twig.extendFilter('isTomorrow', (value) => moment(value).isSame(moment().add(1, 'day'), 'day'))
Twig.extendFilter('dayName', (value) => moment(value).format('dddd'))
Twig.extendFilter('time', (value) => moment(value).format('HH:mm'))

// ==============================
// data

const eventsDirectory = process.env.TRICHTER_EVENTS_DIR ? process.env.TRICHTER_EVENTS_DIR : process.argv[2]
const buildDirectory = process.env.TRICHTER_BUILD_DIR ? process.env.TRICHTER_BUILD_DIR : process.argv[3]

if(!eventsDirectory || !eventsDirectory.trim()) throw new Error('no events path provided. use enviroment variable TRICHTER_EVENTS_DIR or as a command argument')
if(!buildDirectory || !buildDirectory.trim()) throw new Error('no build path provided. use enviroment variable TRICHTER_BUILD_DIR or as a command argument')

const groups = Group.getAll(eventsDirectory)

const events = groups
    // get events from all groups
    .map(group => group.getEvents())

    // reduce it to one array of events
    .reduce((list, groupEvents) => list.concat(groupEvents) , [])

    // multiply events if it is a recurring one
    .map(event => event.recurring ? event.getRecurringEvents() : [event])

    // reduce it to one array of events again
    .reduce((list, entry) => list.concat(entry) , [])

    // filter out past events
    .filter(e => e.end ? e.end.isAfter(moment()) : e.start.add(2, 'hours').isAfter(moment()))


const eventsByDays = groupEventsByDays(events)
const days = Object.keys(eventsByDays)

// ==============================
// render the template

const template = Twig.twig({
    id:   'fs-node-sync',
    path: path.join(__dirname, 'templates/list.twig'),
    async: false
})
fs.writeFileSync(path.join(buildDirectory, 'index.html'), template.render({
    title: 'trichter.cc | Veranstaltungen in Leipzig',
    days: Object.keys(eventsByDays).sort().slice(0,10),
    eventsByDays:  eventsByDays,
}))


