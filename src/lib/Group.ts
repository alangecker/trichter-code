import * as fs from 'fs'
import * as path from 'path'
import * as fm from 'front-matter'
import Event from './Event'

interface IGroupFile {
    name: string
    website: string
    autocrawl?: any
    withoutLabel?: boolean
}

export default class Group {
    key: string
    name: string
    website: string
    autocrawl?: any
    description?: string
    path: string
    constructor(key: string, rootPath: string) {
        this.key = key
        this.path = path.join(rootPath, key)
        try {
            this.load()
        } catch(err) {
            // err.message = 
            throw new Error(`Error while loading '${key}': ${err.message}`)
        }
    }

    static getAll(rootPath: string): Group[] {
        const folders = fs.readdirSync(rootPath).filter(file => file !== 'README.md' && file[0] !== '.')
        return folders.map(folder => new Group(folder, rootPath))
    }
    load() {
        const file = fm(fs.readFileSync(path.join(this.path, 'group.md'), 'utf-8'))
        const frontmatter = <IGroupFile>file.attributes
        
        if(!frontmatter.withoutLabel) {
            if(typeof frontmatter.name != 'string' || !frontmatter.name.trim()) throw new Error('group name is empty, undefined or not a string')
            if(typeof frontmatter.website != 'string' || !frontmatter.website.trim()) throw new Error('group website link is empty, undefined or not a string')
    
            this.name = frontmatter.name
            this.website = frontmatter.website
        }

        if(frontmatter.autocrawl) {
            if(typeof frontmatter.autocrawl !== 'object') throw new Error('invalid autocrawl property')
            if(typeof frontmatter.autocrawl.source != 'string' || !frontmatter.autocrawl.source.trim()) throw new Error('for autocrawling a source must be defined')
            this.autocrawl = frontmatter.autocrawl
        }
        this.description = file.body.trim()
    }

    getEvents(): Event[] {
        const files = fs.readdirSync(this.path).filter(file => file !== 'group.md' && file.match(/\.md$/))

        return files.map( (file) => {
            return Event.load(this, file)
        })
    }
}