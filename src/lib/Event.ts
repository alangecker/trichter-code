import * as fs from 'fs'
import * as path from 'path'
import * as fm from 'front-matter'
import * as moment from 'moment'
import * as yaml from 'js-yaml'
import 'moment-recur-ts'
import Group from './Group'

interface recurringRule {
    units: number|number[],
    measure: 'days'|'weeks'|'months'|'years'|'dayOfWeek'|'dayOfMonth'|'weekOfMonth'|'weekOfYear'|'monthOfYear'
}
interface recurringOptions {
    rules: recurringRule|recurringRule[]
    end?: moment.Moment,
    exceptions?: moment.Moment[]
}

interface IEventFrontmatter {
    id: string
    title: string
    start: string
    end: string
    locationName?: string
    address: string
    link: string
    image?: string
    teaser?: string
    description?: string
    recurring?: recurringOptions
    isCrawled?: boolean
}
export default class Event {
    group: Group
    filename: string = null

    id: string
    title: string
    start: moment.Moment
    end: moment.Moment
    locationName?: string
    address: string
    link?: string
    image?: string
    teaser?: string
    description?: string
    recurring?: recurringOptions
    isCrawled?: boolean

    get editLink(): string {
        return `https://gitlab.com/alangecker/trichter-events/blob/master/${this.group.key}/${encodeURIComponent(this.filename)}`
    }
    constructor(group: Group) {
        this.group = group
    }
    static load(group: Group, filename: string) {
        const e = new Event(group)
        e.filename = filename
        const f: {attributes:IEventFrontmatter, body: string} = fm(fs.readFileSync(path.join(group.path, filename), 'utf8')) 

        e.id = f.attributes.id
        e.title = f.attributes.title
        e.image = f.attributes.image
        e.start = moment(f.attributes.start)
        e.end = f.attributes.end ? moment(f.attributes.end) : null
        e.locationName = f.attributes.locationName
        e.address = f.attributes.address
        e.link = f.attributes.link
        e.image = f.attributes.image
        e.teaser = f.attributes.teaser
        e.recurring = f.attributes.recurring
        e.isCrawled = !!f.attributes.isCrawled
        e.description = f.body.trim()
        e.validate()
        return e
    }

    validate() {
        if(!this.id || (typeof this.id !== 'string' && typeof this.id !== 'number')) throw new Error('id is undefined or invalid')
        if(!this.title || typeof this.title !== 'string' || !this.title.trim()) throw new Error('title is undefined or invalid')
        if(!this.start || !(this.start instanceof moment) || !this.start.isValid()) throw new Error('start time is undefined or invalid')
        // if(!this.address || typeof this.address !== 'string' || !this.address.trim()) throw new Error('address is undefined or invalid')
        
    }
    save() {
        this.validate()
        if(!this.filename) {
            this.filename = `${this.start.format('YYYY-MM-DD')} ${this.title.replace(/([\/\?<>\\:\*\|":]|[\x00-\x1f\x80-\x9f]|^\.+$)/g, '').slice(0,30)}.md`
        }

        const obj: IEventFrontmatter = {
            id: this.id,
            title: this.title,
            start: this.start.format('YYYY-MM-DD HH:mm'),
            end: this.end ? this.end.format('YYYY-MM-DD HH:mm') : null,
            locationName: this.locationName || null,
            address: this.address || null,
            link: this.link || null,
            image: this.image || null,
            teaser: this.teaser || null,
            recurring: this.recurring || null,
            isCrawled: !!this.isCrawled
        }
        // console.log(obj)
        const fileContent = [
            '---',
            yaml.safeDump(obj, {
                lineWidth: 1000
            }).trim(),
            '---',
            this.description
        ].join('\n')

        fs.writeFileSync(path.join(this.group.path, this.filename), fileContent)
    }
    createClone(): Event {
        return  Object.assign(Object.create(Object.getPrototypeOf(this)), this); 
    }

    getRecurringEvents(): Event[] {
        if(!this.recurring) throw new Error('event is not recurring')
        let recur = moment(this.start).recur(Object.assign({start: this.start, end: moment(this.start).add('1', 'year')}, this.recurring))
        let dates = recur.next(10)
        let diff = this.end ? this.end.diff(this.start, 'minutes') : null
        dates.unshift(moment(this.start))

        return dates.map(date => {
            let clone = this.createClone()
            // let clone: Event = Object.assign(Object.create(Object.getPrototypeOf(this)), this); 
            clone.start = date.hours(this.start.utc().hours()).local(true)
            if(this.end) clone.end = moment(date).add(diff, 'minutes').local(true)
            return clone
        })
    }
}