import Event from './Event'
import * as moment from 'moment'

export function groupEventsByDays(events: Event[]): {[date: string]: Event[]} {
    let dates = {}
    
    for(let event of events) {
        let date = moment(event.start).utc().format('YYYY-MM-DD')
        if(!dates[date]) dates[date] = []
        dates[date].push(event)
    }

    // sort by time
    for(let date in dates) dates[date] = dates[date].sort( (a,b) => a.start.unix()-b.start.unix())
    
    return dates
}