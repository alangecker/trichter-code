import Group from './lib/Group'
import Event from './lib/Event'
import facebook_page from './sources/facebook_page'
import ical from './sources/ical'
// import rss from './sources/rss'
import ai1calendar from './sources/ai1calendar'

const sources = {
    facebook_page,
    ical,
    // rss,
    ai1calendar,
}

const rootPath = process.env.TRICHTER_EVENTS_DIR ? process.env.TRICHTER_EVENTS_DIR : process.argv[2]
if(!rootPath || !rootPath.trim()) throw new Error('no events path provided. use enviroment variable TRICHTER_EVENTS_DIR or as a command argument')

const groups = Group.getAll(rootPath)

async function run() {
    for(let group of groups) {
        if(!group.autocrawl) continue
        // if(group.key != 'heinrich-boell-stiftung') continue
        let source = sources[group.autocrawl.source]
        if(!source) {
            console.error(`[${group.key}] invalid source! ignoring`)
            continue
        }

        console.log(`[${group.key}] get events...`)
        try {
            let options = group.autocrawl
            let events: Event[] = await source(options)
            const exclude = options.exclude && options.exclude.length ? options.exclude.map(id => id.toString()) : [] // we use strings for .indexOf()
            for(let event of events) {
                if(exclude.indexOf(event.id.toString()) != -1) continue
                if(options.filter) {
                    let filter = false
                    for(let key in options.filter) {
                        let regex = new RegExp(options.filter[key], 'i')
                        if(!regex.test(event[key])) {
                            filter = true
                            break
                        }
                    }
                    if(filter) continue
                }
                event.group = group
                event.isCrawled = true
                try {
                    event.save()
                } catch(err) {
                    console.error(`[${group.key}] could not save event '${event.title}': ${err.message}`)
                }
            }
        } catch(err) {
            console.log(err)
        }
    }
}


run()