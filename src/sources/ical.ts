import * as ical from 'ical'
import * as moment from 'moment'
import Event from '../lib/Event'

const icalParseURL = (url, options) => new Promise( (resolve,reject) => ical.fromURL(url, options, (err,data) => {
    if(err) reject(err)
    else resolve(data)
}))


export default async function crawl(options: {url:string}): Promise<Event[]> {
    const cal = await icalParseURL(options.url, {})
    return Object.keys(cal).map( (id) => {
        let c = cal[id]
        let event = new Event(null)
        event.id = id.split('@')[0]
        event.title = c.summary
        event.start = moment(c.start)
        if(c.end) event.end = moment(c.end)
        event.address = c.location || null
        event.link = c.url
        event.teaser = c.description ? c.description.substr(0,100) : null
        event.description = c.description.replace(/\n/gm, '\n\n')
        return event
    })
}