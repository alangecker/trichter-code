// import {parseString} from 'xml2js'
import * as cheerio from 'cheerio'
import Event from '../lib/Event'
import fetch from 'node-fetch'


export default async function crawl(options: {url:string}): Promise<Event[]> {
    const res = await fetch(options.url+ `action~agenda/page_offset~1/request_format~html/?request_type=json&ai1ec_doing_ajax=true`)
    let $ = (<any>cheerio).load((await res.json()).html)
    let dates = $('.ai1ec-date')
    dates.each( (index,date) => {
        console.log(date)
        process.exit()

    })
        
    // require('fs').writeFileSync('/tmp/x2.html', (await res.json()).html)
    return []
}