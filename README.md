# trichter.cc
*An independent, free platform for collecting and networking events & groups in Leipzig.*

## Installing
```bash
$ git clone https://gitlab.com/alangecker/trichter-code.git trichter
$ cd trichter
$ git clone https://gitlab.com/alangecker/trichter-events.git events
$ yarn
```

## Using

### Crawling (optional)
`$ yarn crawl` 

It crawles events from various sources and writes them into the events directory.

#### Currently supported crawling sources
- Facebook \
    *Note: the `src/sources/facebook_page.ts` is not included in the repository. because of legal reasons it is not possible to release it as open source for now. ...but write me... ;)* 
- iCal


### Building
`$ yarn build`

It generates static html code in the `build/` directory based on the events. Additionally it compiles scss and copies assets to the same directory.


### Crawling & Building combined
`$ yarn update-with-git` 

##### it
1. pulls changes from the events-repo
2. crawles for new events
3. pushes them again to the remote repo
4. rebuilds the static html code

### Serving locally
`$ cd build/ && python3 -m http.server 8080`

runs a local webserver: http://localhost:8080
